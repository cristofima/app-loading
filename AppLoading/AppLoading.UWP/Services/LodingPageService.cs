﻿using AppLoading.Interfaces;
using AppLoading.UWP.Services;
using AppLoading.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

using XFPlatform = Xamarin.Forms.Platform.UWP.Platform;

[assembly: Dependency(typeof(LodingPageService))]

namespace AppLoading.UWP.Services
{
    public class LodingPageService : ILodingPageService
    {
        private Windows.UI.Xaml.FrameworkElement _nativeView;

        private bool _isInitialized;

        private LoadingPage loading { get; set; }

        public void InitLoadingPage(ContentPage loadingIndicatorPage = null)
        {
            // check if the page parameter is available
            if (loadingIndicatorPage != null)
            {
                loading = new LoadingPage();
                // build the loading page with native base
                var renderer = loadingIndicatorPage.GetOrCreateRenderer();

                _nativeView = renderer.ContainerElement;

                _isInitialized = true;
            }
        }

        public void ShowLoadingPage()
        {
            // check if the user has set the page or not
            if (!_isInitialized)
                InitLoadingPage(new LoadingIndicatorPage1()); // set the default page
        }

        private void XamFormsPage_Appearing(object sender, EventArgs e)
        {
            var animation = new Animation(callback: d => ((ContentPage)sender).Content.Rotation = d,
                                          start: ((ContentPage)sender).Content.Rotation,
                                          end: ((ContentPage)sender).Content.Rotation + 360,
                                          easing: Easing.Linear);
            animation.Commit(((ContentPage)sender).Content, "RotationLoopAnimation", 16, 800, null, null, () => true);
        }

        public void HideLoadingPage()
        {
            // Hide the page
        }
    }

    internal static class PlatformExtension
    {
        public static IVisualElementRenderer GetOrCreateRenderer(this VisualElement bindable)
        {
            var renderer = XFPlatform.GetRenderer(bindable);
            if (renderer == null)
            {
                renderer = XFPlatform.CreateRenderer(bindable);
                XFPlatform.SetRenderer(bindable, renderer);
            }
            return renderer;
        }
    }
}